    @extends('principal')
    @section('contenido')
        <template v-if="menu==0">
            <inicio></inicio>
        </template>

        <template v-if="menu==1">
            <examenes></examenes>
        </template>

        <template v-if="menu==2">
            <grupos></grupos>
        </template>

        <template v-if="menu==3">
            <materias></materias>
        </template>

        <template v-if="menu==4">
            <alumnos></alumnos>
        </template>

        <template v-if="menu==5">
            <preguntas></preguntas>
        </template>
        
    @endsection