
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('inicio', require('./components/Inicio.vue'));
Vue.component('examenes', require('./components/Examenes.vue'));
Vue.component('grupos', require('./components/Grupos.vue'));
Vue.component('materias', require('./components/Materias.vue'));
Vue.component('alumnos', require('./components/Alumnos.vue'));
Vue.component('preguntas', require('./components/Preguntas.vue'));

const app = new Vue({
    el: '#app',
    data :{
        menu : 0
    }
});
